jQuery(document).ready(function($) {
    
    $('.icf_form').on('submit', function(e) {
        e.preventDefault();
        
        let error = false;
        let title = $('#icf_field_title').val();
        let message = $('#icf_field_message').val();

        if (title == '' ) { error = true; }

        if (!error) {
            console.log(title, message);

            jQuery.ajax({
                type: "POST",
                url: icf_ajax_script.ajaxurl,
                data: {
                    action: 'icf_sendform',
                    title: title,
                    message: message
                },
                success: function (response) {
                    $('.icf_form').empty();
                    $('.icf_form_result').html(response);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.icf_form_result').html(errorThrown);
                }
            });

        } else {
            console.log('error');
        }
    })
});