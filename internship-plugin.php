<?php

/**
 * Plugin Name: Internship Plugin
 * Description: Internship plugin description 
 * Author: Oleg Draganchuk
 * Version: 1.0
 */

 // add assets
add_action( 'wp_enqueue_scripts', 'icf_enqueue_styles_and_scripts');

function icf_enqueue_styles_and_scripts() {
    wp_enqueue_style('icf-styles', plugin_dir_url(__FILE__).'/css/style.css', array(), '1.0.0', 'all');

    wp_enqueue_script('jquery');
    
    wp_enqueue_script('icf-scripts', plugin_dir_url(__FILE__).'/js/scripts.js', array('jquery'), '1.0.0', true);
    wp_localize_script('icf-scripts', 'icf_ajax_script', array('ajaxurl' =>admin_url('admin-ajax.php')));
}


//add sortcode
add_shortcode( 'internship-contact-form', 'icf_shortcode_function' );
function icf_shortcode_function($args) {

    $args = shortcode_atts([
        'title' => 'WordPress.org',
    ], $args);

    ob_start();
    ?>
        <div class="icf_form_wrapper">
            <h2><?php echo $args['title']; ?></h2>
            <form class="icf_form">
                <div><input type="text" id="icf_field_title" name="name" placeholder="title"></div>
                <div><textarea name="message" id="icf_field_message" placeholder="message"></textarea></div>
                <div><input type="submit" value="send"></div>
            </form>
            <div class="icf_form_result"></div>
        </div>
    <?
    $html = ob_get_clean();
    return $html;
}

// add wp_ajax action

add_action('wp_ajax_nopriv_icf_sendform', 'icf_sendform_function');
add_action('wp_ajax_icf_sendform', 'icf_sendform_function');

function icf_sendform_function() {
    $args = $_REQUEST;
    
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $result = wp_mail( 'oleg.draganchuk@gmail.com', $args['title'], $args['message'], $headers );

    if ($result) {
        echo 'Message sent';
    } else {
        echo 'Error';
    }
    wp_die();
}
